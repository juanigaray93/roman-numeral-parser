type RomanNumeralSymbol = "I" | "V" | "X" | "L" | "C" | "D" | "M";
type RomanNumber = `${RomanNumeralSymbol}${RomanNumeralSymbol | ""}${
  | RomanNumeralSymbol
  | ""}${RomanNumeralSymbol | ""}${RomanNumeralSymbol | ""}`;
const SymbolValues: Record<RomanNumeralSymbol, number> = {
  I: 1,
  V: 5,
  X: 10,
  L: 50,
  C: 100,
  D: 500,
  M: 1000,
};
const divideNumberInGreatestSymbolsThenPrefixesAndSuffixes = (
  number: RomanNumber
): [RomanNumber | "", RomanNumber, RomanNumber | ""] => {
  const symbols = number.split("") as RomanNumeralSymbol[];
  // Find largest symbol.
  let largestSymbol: RomanNumeralSymbol | null = null;
  symbols.forEach((symbol) => {
    const symbolValue = SymbolValues[symbol];
    if (largestSymbol === null || symbolValue > SymbolValues[largestSymbol]) {
      largestSymbol = symbol;
    }
  });

  // Find place where that largest symbol is clustered.
  const firstIndex = symbols.findIndex((symbol) => symbol === largestSymbol);
  let lastIndex = firstIndex;
  while (symbols[lastIndex + 1] === largestSymbol) {
    lastIndex++;
  }

  // Separate three slices of the symbols array.
  const prefixes = firstIndex > 0 ? symbols.slice(0, firstIndex) : [];
  const greatestSymbols = symbols.slice(firstIndex, lastIndex + 1); // The "end" is not included in the slice
  const suffixes =
    lastIndex !== symbols.length
      ? symbols.slice(lastIndex + 1, symbols.length + 1)
      : [];

  return [
    prefixes.join("") as RomanNumber | "",
    greatestSymbols.join("") as RomanNumber,
    suffixes.join("") as RomanNumber | "",
  ];
};
const addUpRomanNumerals = (numerals: RomanNumeralSymbol[]) => {
  return numerals.reduce((accum, curr) => {
    return accum + SymbolValues[curr];
  }, 0);
};
const parseRomanNumber = (number: RomanNumber | ""): number => {
  if (number === "") return 0;
  const [leftSide, greatestSymbols, rightSide] =
    divideNumberInGreatestSymbolsThenPrefixesAndSuffixes(number);
  const valueOfLeftSide = parseRomanNumber(leftSide);
  const valueOfRightSide = parseRomanNumber(rightSide);
  const valueOfMiddlePortion = addUpRomanNumerals(
    greatestSymbols.split("") as RomanNumeralSymbol[]
  );
  return valueOfMiddlePortion - valueOfLeftSide + valueOfRightSide;
};

const expectEquality =
  (description: string) =>
  <T>(obtained: T, expected: T): boolean => {
    if (obtained !== expected) {
      console.error(
        "Problem in " +
          description +
          ": these two are different! " +
          JSON.stringify({ obtained, expected }, null, 2)
      );
      return false;
    }
    console.log(description + " is ok.");
    return true;
  };
const results = [
  expectEquality("I")(parseRomanNumber("I"), 1),
  expectEquality("II")(parseRomanNumber("II"), 2),
  expectEquality("III")(parseRomanNumber("III"), 3),
  expectEquality("IIII")(parseRomanNumber("IIII"), 4), // This should work for non-standard notations!
  expectEquality("IV")(parseRomanNumber("IV"), 4),
  expectEquality("V")(parseRomanNumber("V"), 5),
  expectEquality("VI")(parseRomanNumber("VI"), 6),
  expectEquality("VII")(parseRomanNumber("VII"), 7),
  expectEquality("VIII")(parseRomanNumber("VIII"), 8),
  expectEquality("IX")(parseRomanNumber("IX"), 9),
  expectEquality("X")(parseRomanNumber("X"), 10),
  expectEquality("XIX")(parseRomanNumber("XIX"), 19),
  expectEquality("XXIX")(parseRomanNumber("XXIX"), 29),
  expectEquality("XLIX")(parseRomanNumber("XLIX"), 49),
  expectEquality("L")(parseRomanNumber("L"), 50),
  expectEquality("MXCIX")(parseRomanNumber("MXCIX"), 1099),
  expectEquality("MCMXC")(parseRomanNumber("MCMXC"), 1990),
].reduce(
  (acc, curr) => {
    if (curr) {
      return {
        failed: acc.failed,
        successful: acc.successful + 1,
      };
    }
    return {
      successful: acc.successful,
      failed: acc.failed + 1,
    };
  },
  {
    successful: 0,
    failed: 0,
  }
);
console.table(results);
